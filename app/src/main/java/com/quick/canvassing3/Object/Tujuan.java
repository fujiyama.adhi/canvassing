package com.quick.canvassing3.Object;

public class Tujuan {

    private String id_kirim;
    private double lat;
    private double lng;
//    private double jarak;

    //    public double getJarak() {
//        return jarak;
//    }
//
//    public void setJarak(double jarak) {
//        this.jarak = jarak;
//    }

    public Tujuan(String id_kirim, double lat, double lng) {
        super();
        this.id_kirim = id_kirim;
        this.lat = lat;
        this.lng = lng;
//        this.jarak = jarak;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getId_kirim() {

        return id_kirim;
    }

    public void setId_kirim(String id_kirim) {
        this.id_kirim = id_kirim;
    }
}
