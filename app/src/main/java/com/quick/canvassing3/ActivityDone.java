package com.quick.canvassing3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class ActivityDone extends AppCompatActivity {
    Button bt_ok;
    spHelp SP_Helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done);

        SP_Helper = new spHelp(this);

        bt_ok = (Button) findViewById(R.id.bt_ok);

        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SP_Helper.saveSPBoolean(spHelp.SP_SUDAH_MANGKAT, false);
                finish();
            }
        });
    }
}
