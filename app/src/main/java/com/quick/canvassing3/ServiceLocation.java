package com.quick.canvassing3;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;

import com.quick.canvassing3.Config.Config;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;

public class ServiceLocation extends Service {
    Runnable run;
    Handler handler;
    spHelp SP_Help;
    dbHelp DB_Help;
    InputStream is = null;
    String username, rute_id, result = null, line = null;
    int duration;

    public ServiceLocation() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //if condition for connection internet
        if (isConnect()) {
            notifForeground("Connected");
        } else {
            notifForeground("Disconnect");
        }
        allowNetworkOnMainThread();
        //get value
        SP_Help = new spHelp(this);
        username = SP_Help.getSPUsername();
        rute_id = SP_Help.getSPID();
        duration = SP_Help.getSPDurasi();

        handler = new Handler();
        DB_Help = new dbHelp(getApplicationContext());
        //start runnable get location & upload
        runFunc();
    }

    void runFunc() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getLocation();
                run = this;
                handler.postDelayed(run, duration);//2 menit
            }
        }, 5000);
    }

    //function get location & upload loacation
    void getLocation() {
        GPSTracker gps = new GPSTracker(this);
        //if connect save location sqlite, upload location sql, delete location sqlite
        if (isConnect() && gps.canGetLocation()) {
            notifForeground("Connected");
            insertSQLite(gps);
            Cursor c = DB_Help.selectData();
            while (c.moveToNext()) {
                int id = c.getInt(0);
                String latitude = c.getString(c.getColumnIndex(DB_Help.C1_LATITUDE));
                String longitude = c.getString(c.getColumnIndex(DB_Help.C2_LONGITUDE));
                String time = c.getString(c.getColumnIndex(DB_Help.C3_TIME));

                Log.d("TIME ", time);
                upload(rute_id, latitude, longitude, time);
                Log.e("UPLOAD", rute_id + "|" + latitude + "|" + longitude + "|" + time);
                DB_Help.deleteRow(id);
            }
        }
        //if not connect save location sqlite
        else if (!isConnect() && gps.canGetLocation()) {
            insertSQLite(gps);
            notifForeground("Disconnect");
        }
    }

    //function insert location to sqlite
    void insertSQLite(GPSTracker gps) {
        Calendar cal = Calendar.getInstance();
        int date = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH);
        month = month + 1;
        int year = cal.get(Calendar.YEAR);

        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        int sec = cal.get(Calendar.SECOND);

        ContentValues values = new ContentValues();
        values.put(DB_Help.C1_LATITUDE, gps.getLatitude());
        values.put(DB_Help.C2_LONGITUDE, gps.getLongitude());
        if (month < 10 && sec < 10) {
            values.put(DB_Help.C3_TIME, "" + year + "-0" + month + "-" + date + " " + hour + ":" + min + ":0" + sec);
        } else if (month < 10) {
            values.put(DB_Help.C3_TIME, "" + year + "-0" + month + "-" + date + " " + hour + ":" + min + ":" + sec);
        } else if (sec < 10) {
            values.put(DB_Help.C3_TIME, "" + year + "-" + month + "-" + date + " " + hour + ":" + min + ":0" + sec);
        } else {
            values.put(DB_Help.C3_TIME, "" + year + "-" + month + "-" + date + " " + hour + ":" + min + ":" + sec);
        }
        DB_Help.insertLocation(values);
    }

    //function post value location to php
    void upload(String rute_id, String latitude, String longitude, String time) {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        nameValuePairs.add(new BasicNameValuePair("rute_id", rute_id));
        nameValuePairs.add(new BasicNameValuePair("latitude", latitude));
        nameValuePairs.add(new BasicNameValuePair("longitude", longitude));
        nameValuePairs.add(new BasicNameValuePair("time", time));

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.UPLOAD_LOCATION);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString());
        }

        try {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("Update", "connection status : " + result);
            Log.e("Update", "QUERY : " + result);
        } catch (Exception e) {
            Log.e("Fail 2", e.toString());
        }
    }

    //function checking internet connection
    boolean isConnect() {
        ConnectivityManager conMngr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMngr.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    //set network on main thread
    void allowNetworkOnMainThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    //function notif foregroun to make sarvice never close
    private void notifForeground(String status) {
        Intent notificationIntent = new Intent(this, ActivityLogin.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Notification notification = new android.support.v7.app.NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.toko_quick)
                .setContentTitle("Canvassing")
                .setContentText(status)
                .setContentIntent(pendingIntent).build();

        startForeground(1337, notification);
    }
}
