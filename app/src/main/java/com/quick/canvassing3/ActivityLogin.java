package com.quick.canvassing3;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.quick.canvassing3.Config.Config;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class ActivityLogin extends AppCompatActivity {
    String line = null;
    String result_login, nama, username, password;
    int duration;
    JSONArray resultJson = null;
    Boolean connect_status = null;

    Button bt_login;
    EditText et_username, et_password;
    ImageView logo;
    LinearLayout l_login;
    Animation fromtop, frombottom;
    ProgressBar pb_login;

    spHelp SP_Helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        allowNetworkOnMainThread();

        //init sphelper for session login
        SP_Helper = new spHelp(this);
        //if condition for session login
        //already login and already start maps
        if (SP_Helper.getSPSudahLogin() && SP_Helper.getSPSudahMangkat()) {
            Intent i = new Intent(ActivityLogin.this, ActivityLoading.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra("mode", 1);
            startActivity(i);
            finish();
        }
        //already login but not yet start mpas
        else if (SP_Helper.getSPSudahLogin() && !SP_Helper.getSPSudahMangkat()) {
            startActivity(new Intent(ActivityLogin.this, ActivityMain.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        } else {
        }
        //init ui
        bt_login = (Button) findViewById(R.id.bt_login);
        logo = (ImageView) findViewById(R.id.iv_login_logo);
        l_login = (LinearLayout) findViewById(R.id.layout_login);
        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);
        pb_login = (ProgressBar) findViewById(R.id.pb_login);
        //init animation
        logo.setAnimation(fromtop);
        l_login.setAnimation(frombottom);
        //listener for button login
        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = et_username.getText().toString();
                password = et_password.getText().toString();
                //if condition for empty username or password
                if (username.isEmpty()) {
                    et_username.setError("enter a valid username");
                    et_username.requestFocus();
                } else if (password.isEmpty()) {
                    et_password.setError("enter a valid password");
                    et_password.requestFocus();
                } else {
                    pb_login.setVisibility(View.VISIBLE);
                    ConnectionCheck();
                }
            }
        });
        //hide actionbar
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
    }

    //function post for login
    public void login() {
        class GetDataJSON extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("username", username));
                nameValuePairs.add(new BasicNameValuePair("password", password));
                nameValuePairs.add(new BasicNameValuePair("device", "android"));

                InputStream inputStream = null;
                String result = null;
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(Config.LOGIN);
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();

                    inputStream = entity.getContent();

                    Log.e("pass 1", "connection success ");
                } catch (Exception e) {
                    Log.e("Fail 1", e.toString());
                }

                try {
                    BufferedReader reader = new BufferedReader
                            (new InputStreamReader(inputStream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    inputStream.close();
                    result = sb.toString();
                    result_login = result;
                    Log.e("pass 2", "connection success : " + result);
                } catch (Exception e) {
                    Log.e("Fail 2", e.toString());
                }
                Log.e("Result ", result);
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                result_login = result;

                action();
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }

    //handle result login from post
    protected void action() {
        String c = Character.toString(result_login.charAt(0));
        if (c.equals("0")) {
            et_username.setError("enter a valid username");
            et_password.setError("enter a valid password");
            et_username.requestFocus();
            pb_login.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), "Akun atau Password yang anda masukkan salah", Toast.LENGTH_SHORT).show();
        } else {
            try {
                JSONObject jsonObj = new JSONObject(result_login);
                resultJson = jsonObj.getJSONArray("result");

                JSONObject d = resultJson.getJSONObject(0);

                nama = d.getString("nama");
                duration = d.getInt("durasi");

                // Stop refresh animation
            } catch (JSONException e) {
                e.printStackTrace();
            }


            Log.d("NAMA", nama + "\n" + duration);
            SP_Helper.saveSPInt(spHelp.SP_DURATION, duration);
            SP_Helper.saveSPString(spHelp.SP_USERNAME, username);
            SP_Helper.saveSPString(spHelp.SP_NAMA, nama.replaceAll("\\s+$", ""));
            SP_Helper.saveSPBoolean(spHelp.SP_SUDAH_LOGIN, true);
            Intent i = new Intent(ActivityLogin.this, ActivityMain.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            finish();
        }
    }

    void ConnectionCheck(){
        connect_status = null;
        try {
            connect_status = getServerContentsJson();
        } catch (IOException e) {
            Toast.makeText(this, " Eror", Toast.LENGTH_SHORT).show();
        }
        Log.e("STATUS UTAMA", "" + connect_status);

        if (connect_status){
            login();
        }else {
            Toast.makeText(this, "Anda tidak terkoneksi dengan server", Toast.LENGTH_SHORT).show();
            pb_login.setVisibility(View.GONE);
        }
    }

    private Boolean getServerContentsJson()
            throws IOException {
        HttpClient httpclient = new DefaultHttpClient();

        Boolean responseString = null;
        HttpResponse response = null;
        try {
            response = httpclient.execute(new HttpGet(Config.LOGIN));

            StatusLine statusline = response.getStatusLine();

            if (statusline.getStatusCode() == HttpStatus.SC_OK) {
                responseString = true;
                return responseString;
            } else {
                responseString = false;
            }
        } catch (IOException e) {
            responseString = false;
        } finally {
            if (response != null) {
                response.getEntity().consumeContent();
            }
        }
        return responseString;
    }

    void allowNetworkOnMainThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    //back press button on android
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Anda yakin ingin menutup aplikasi ini?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }
}
