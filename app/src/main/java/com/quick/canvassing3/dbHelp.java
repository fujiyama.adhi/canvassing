package com.quick.canvassing3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class dbHelp extends SQLiteOpenHelper {
    final String TABLE_LOCATION = "tb_location";
    final String C1_LATITUDE = "latitude";
    final String C2_LONGITUDE = "longitude";
    final String C3_TIME = "time";
    String mQuery;

    public dbHelp(Context context) {
        super(context, "db_location", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        mQuery = "CREATE TABLE IF NOT EXISTS " + TABLE_LOCATION + " (" +
                "_id INTEGER PRIMARY KEY," +
                "latitude TEXT," +
                "longitude TEXT," +
                "time TEXT)";
        db.execSQL(mQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    void insertLocation(ContentValues values) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_LOCATION, null, values);
    }

    Cursor selectData() {
        SQLiteDatabase db = this.getWritableDatabase();
        mQuery = "SELECT * FROM " + TABLE_LOCATION;
        return db.rawQuery(mQuery, null);
    }

    void deleteRow(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        mQuery = "DELETE FROM " + TABLE_LOCATION + " WHERE _id=" + id;
        db.execSQL(mQuery);
    }
}
