package com.quick.canvassing3;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.quick.canvassing3.Config.Config;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ActivityLoading extends AppCompatActivity {
    spHelp SP_Helper;
    String rute_id, data_result;
    JSONArray resultJson1 = null;
    ArrayList<String> alias, alamat, desty_display, latitude, longitude, noind, nama, name_display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        //get value id rute
        SP_Helper = new spHelp(this);
        rute_id = SP_Helper.getSPID();
        //start function get value for list
        getList();
    }

    //funtion post to get value for list
    void getList() {
        class GetDataJSON extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("id_rute", rute_id));

                String result = null;
                InputStream is = null;
                String line;
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(Config.GET_DETAIL);
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                    Log.e("pass 1", "connection success ");
                } catch (Exception e) {
                    Log.e("Fail 1", e.toString());
                }
                try {
                    BufferedReader reader = new BufferedReader
                            (new InputStreamReader(is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    result = sb.toString();
                    Log.e("pass 2", "connection status : " + result);
                } catch (Exception e) {
                    Log.e("Fail 2", e.toString());
                }
                Log.e("Result", result);
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                data_result = result;
                if (data_result.length() > 1) {
                    save_object();
                } else {
                    Toast.makeText(ActivityLoading.this, "KOSONG", Toast.LENGTH_SHORT).show();
                }
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }

    //handle convert json object to array
    void save_object() {
        alias = new ArrayList<String>();
        alamat = new ArrayList<String>();
        latitude = new ArrayList<String>();
        longitude = new ArrayList<String>();
        desty_display = new ArrayList<String>();
        noind = new ArrayList<String>();
        nama = new ArrayList<String>();
        name_display = new ArrayList<String>();

        try {
            JSONObject jsonObj = new JSONObject(data_result);
            resultJson1 = jsonObj.getJSONArray("destinasi");
            for (int i = 0; i < resultJson1.length(); i++) {
                JSONObject c = resultJson1.getJSONObject(i);
                alias.add(c.getString("alias"));
                alamat.add(c.getString("alamat"));
                latitude.add(c.getString("latitude"));
                longitude.add(c.getString("longitude"));

                desty_display.add("" + (i + 1) + " - " + c.getString("alias"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Intent c = new Intent(ActivityLoading.this, ActivityMaps.class);
        c.putExtra("rute_canvassing_id", rute_id);
        c.putExtra("mode", 1);

        c.putStringArrayListExtra("alias", alias);
        c.putStringArrayListExtra("alamat", alamat);
        c.putStringArrayListExtra("display", desty_display);
        c.putStringArrayListExtra("latitude", latitude);
        c.putStringArrayListExtra("longitude", longitude);

        startActivity(c);
        finish();
    }
}
