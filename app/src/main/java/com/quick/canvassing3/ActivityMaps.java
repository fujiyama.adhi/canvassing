package com.quick.canvassing3;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ebanx.swipebtn.OnStateChangeListener;
import com.ebanx.swipebtn.SwipeButton;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.quick.canvassing3.Config.Config;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ActivityMaps extends AppCompatActivity implements OnMapReadyCallback {

    ArrayList<String> alias, alamat, latitude, longitude, desty_display;
    String url, rute_canvassing_id, username, nama, result_passwd;
    String result = null, line = null;
    double latA, lngA, latCur, lngCur;
    int mode, padding = 50, gambar;
    float blue = 240.0f, red = 0.0f;
    LinearLayout ll_status;
    Button bt_startMaps;
    SwipeButton btn_swipe;
    ImageView slideImage;
    TextView text_status, text_slide;
    FloatingActionButton fb_current;
    LatLng latLng;
    Marker cLoc, dLoc;
    Polyline jalur;
    Runnable run;
    Handler handler;
    spHelp SP_Helper;
    InputStream is = null;
    OnStateChangeListener listen;
    private GoogleMap mMap;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        allowNetworkOnMainThread();


        //listener for swipe button
        listen = new OnStateChangeListener() {
            @Override
            public void onStateChange(final boolean active) {
                if (active) {
                    new AlertDialog.Builder(ActivityMaps.this)
                            .setMessage("Apakah perjalanan anda sudah selesai?")
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialogAlasan("Masukkan password anda", "Password anda salah");
                                }
                            })
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    collapseButton();
                                }
                            })
                            .show();
                }
            }
        };

        handler = new Handler();
        SP_Helper = new spHelp(this);

        //get value
        username = SP_Helper.getSPUsername();
        nama = SP_Helper.getSPNama();
        mode = getIntent().getIntExtra("mode", -1);
        latitude = getIntent().getStringArrayListExtra("latitude");
        longitude = getIntent().getStringArrayListExtra("longitude");
        alias = getIntent().getStringArrayListExtra("alias");
        alamat = getIntent().getStringArrayListExtra("alamat");
        desty_display = getIntent().getStringArrayListExtra("display");
        rute_canvassing_id = getIntent().getStringExtra("rute_canvassing_id");

        //set title
        setTitle(R.string.title_activity_preview);

        //switch display
        if (mode == 0) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //init ui
        btn_swipe = (SwipeButton) findViewById(R.id.btn_swipe);
        bt_startMaps = (Button) findViewById(R.id.bt_startMaps);
        text_status = (TextView) findViewById(R.id.text_status);
        fb_current = (FloatingActionButton) findViewById(R.id.fb_current);
        ll_status = (LinearLayout) findViewById(R.id.ll_status);
        final RelativeLayout rl_root = (RelativeLayout) btn_swipe.getChildAt(0);

        slideImage = (ImageView) btn_swipe.getChildAt(1);
        text_slide = (TextView) rl_root.getChildAt(0);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                gambar = slideImage.getWidth();
            }
        }, 1000);

        text_status.setText("Rute Canvassing " + rute_canvassing_id);

        bt_startMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityMaps.this, ActivityMaps.class);
                mode = 1;
                i.putExtra("mode", mode);
                startActivity(i);
                finish();
            }
        });

        btn_swipe.setOnStateChangeListener(listen);

        //listener for floating button to current location
        fb_current.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updategetLocation();
                camera(latCur, lngCur);
            }
        });

        //listener to status alias on linearlayout
        ll_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRute(rute_canvassing_id);
            }
        });
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void collapseButton() {
        final ValueAnimator widthAnimator = ValueAnimator.ofInt(
                btn_swipe.getWidth(),
                gambar);

        widthAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ViewGroup.LayoutParams params = slideImage.getLayoutParams();
                params.width = (Integer) widthAnimator.getAnimatedValue();
                slideImage.setLayoutParams(params);
            }
        });

        widthAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Drawable forward = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_forward);
                slideImage.setImageDrawable(forward);
                listen.onStateChange(false);
                text_slide.setText("GESER KETIKA SAMPAI");
            }
        });

        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(
                text_slide, "alpha", 1);

        AnimatorSet animatorSet = new AnimatorSet();

        animatorSet.playTogether(objectAnimator, widthAnimator);
        animatorSet.start();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        //switch display maps
        if (mode == 1 && !SP_Helper.getSPSudahMangkat()) {
            //call get location function
            getLocation();
            //set title action bar
            setTitle(R.string.title_activity_maps);
            btn_swipe.setVisibility(View.VISIBLE);
            bt_startMaps.setVisibility(View.GONE);
            //start service
            startService(new Intent(getBaseContext(), ServiceLocation.class));
            //save session
            SP_Helper.saveSPString(spHelp.SP_ID, rute_canvassing_id);
            SP_Helper.saveSPBoolean(spHelp.SP_SUDAH_MANGKAT, true);
            SP_Helper.saveSPString(spHelp.SP_LAT, String.valueOf(latA));
            SP_Helper.saveSPString(spHelp.SP_LNG, String.valueOf(lngA));
        } else if (mode == 1 && SP_Helper.getSPSudahMangkat()) {
            setTitle(R.string.title_activity_maps);
            latA = Double.parseDouble(SP_Helper.getSpLat());
            lngA = Double.parseDouble(SP_Helper.getSpLng());
            //start service
            startService(new Intent(getBaseContext(), ServiceLocation.class));
            btn_swipe.setVisibility(View.VISIBLE);
            bt_startMaps.setVisibility(View.GONE);
        } else {
            getLocation();
        }

        //init google maps
        mMap = googleMap;

        //call runnable get location function
        runFunc();
        //maps permission
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            }else {
                requestPermissions();

            }
        }

        if (isConnectInternet()) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            //init current location marker to fit camera
            builder.include(new LatLng(latA, lngA));

            //looping for draw camera and polyline
            for (int i = 0; i < (desty_display.size()); i++) {
                dLoc = marker(Double.parseDouble(latitude.get(i)), Double.parseDouble(longitude.get(i)), false, alias.get(i), alamat.get(i));

                //init all marker to fit camera
                builder.include(new LatLng(Double.parseDouble(latitude.get(i)), Double.parseDouble(longitude.get(i))));
                //move camera to fit all marker
                LatLngBounds bounds = builder.build();
                final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);

                //call draw polyline
                if (i < (desty_display.size() - 1)) {
                    url = getUrl(Double.parseDouble(latitude.get(i)), Double.parseDouble(longitude.get(i)), Double.parseDouble(latitude.get(i + 1)), Double.parseDouble(longitude.get(i + 1)));
                } else {
                    url = getUrl(Double.parseDouble(latitude.get(0)), Double.parseDouble(longitude.get(0)), Double.parseDouble(latitude.get(i)), Double.parseDouble(longitude.get(i)));
                }
                FetchUrl FetchUrl = new FetchUrl();
                FetchUrl.execute(url);

                mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        mMap.animateCamera(cu, 3000, null);
                    }
                });

                //delay looping
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    private void requestPermissions() {

        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION);

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);


        // Provide an additional rationale to the img_user. This would happen if the img_user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale || shouldProvideRationale2) {
            showSnackbar(R.string.String_permission,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(ActivityMaps.this,
                                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the img_user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(ActivityMaps.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    //
    void getLocation() {
        GPSTracker gps = new GPSTracker(this);
        if (gps.canGetLocation()) {
            latA = gps.getLatitude();
            lngA = gps.getLongitude();
//            if (cLoc != null) {
//                cLoc.remove();
//                cLoc = marker(latA, lngA, true, "Saya", username);
//            }else {
//                cLoc = marker(latA, lngA, true, "Saya", username);
//            }
//            cLoc = marker(latA, lngA, true, "Saya", nama);
//            camera(latA,lngA);
//            Toast.makeText(this, ""+latA+"\n"+""+lngA, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Can't get location", Toast.LENGTH_SHORT).show();
        }
    }

    //function to get current location
    void updategetLocation() {
        GPSTracker gps = new GPSTracker(this);
        if (gps.canGetLocation()) {
            latCur = gps.getLatitude();
            lngCur = gps.getLongitude();
//            if (cLoc != null) {
//                cLoc.remove();
//                cLoc = marker(latA, lngA, true, "Saya", username);
//            }else {
//                cLoc = marker(latA, lngA, true, "Saya", username);
//            }
//            cLoc = marker(latA, lngA, true, "Saya", nama);
//            camera(latA,lngA);
//            Toast.makeText(this, ""+latA+"\n"+""+lngA, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Can't get location", Toast.LENGTH_SHORT).show();
        }
    }

    //looping for get current location
    void runFunc() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                updategetLocation();
                run = this;
                handler.postDelayed(run, 5000);
            }
        }, 5000);
    }

    //Fungsi draw marker
    public Marker marker(double lat, double lng, boolean mark_type, String subjek, String alamat) {
        latLng = new LatLng(lat, lng);
//        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
//        Bitmap bmp = Bitmap.createBitmap(200, 50, conf);
//        Canvas canvas = new Canvas(bmp);
//
//        Paint color = new Paint();
//        color.setTextSize(35);
//        color.setColor(Color.BLACK);
//
//        canvas.drawText("1", 0, 50, color);

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(subjek);
        markerOptions.snippet(alamat);
        if (mark_type) {
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(blue));
        } else {
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(red));
//            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bmp));
        }

        return mMap.addMarker(markerOptions);
    }

    //Fungsi animasi kamera
    void camera(double lat, double lng) {
        CameraPosition newCamPos = new CameraPosition(new LatLng(lat, lng),
                15.5f,
                mMap.getCameraPosition().tilt, //use old tilt
                mMap.getCameraPosition().bearing); //use old bearing
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 3000, null);
    }

    //Fungsi draw polyline-->
    private String getUrl(double latA, double lngA, double latB, double lngB) {

        // Origin of route
        String str_origin = "origin=" + latA + "," + lngA;

        // Destination of route
        String str_dest = "destination=" + latB + "," + lngB;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        // Building the url to the web service

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    //function upload status finish
    void finish(String rute_id, String status, String komen) {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        nameValuePairs.add(new BasicNameValuePair("rute_id", rute_id));
        nameValuePairs.add(new BasicNameValuePair("status", status));
        nameValuePairs.add(new BasicNameValuePair("komen", komen));

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.FINISH);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString());
        }

        try {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection status : " + result);
        } catch (Exception e) {
            Log.e("Fail 2", e.toString());
        }
        hasil(result);
    }

    void hasil(String result) {
        if (result.length() == 1) {
            Toast.makeText(this, "Koneksi bermasalah, silahkan ulangi lagi", Toast.LENGTH_SHORT).show();
        } else {
            Intent i = new Intent(ActivityMaps.this, ActivityDone.class);
            stopService(new Intent(getBaseContext(), ServiceLocation.class));
            startActivity(i);
            finish();
        }
    }
    //<--Fungsi draw polyline

    //function dialog all alias on rute
    void dialogRute(String rute_canvassing_id) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View viewInflater = inflater.inflate(R.layout.dialog_rute, null, false);
        final ListView lv_rute = (ListView) viewInflater.findViewById(R.id.lv_rute);
        final ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, R.layout.row_desty, desty_display);

        lv_rute.setAdapter(adapter1);

        lv_rute.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                camera(Double.parseDouble(latitude.get(position)), Double.parseDouble(longitude.get(position)));
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Rute " + rute_canvassing_id)
                .setView(viewInflater)
                .setCancelable(true)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).create().show();
    }

    //function dialog input password
    void dialogAlasan(String alasan, final String snackbar) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View viewInflate = inflater.inflate(R.layout.dialog_text, (ViewGroup) findViewById(android.R.id.content), false);
        final EditText et_alasan = (EditText) viewInflate.findViewById(R.id.et_alasan);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(alasan)
                .setView(viewInflate)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String s_alasan = et_alasan.getText().toString();
                        if (s_alasan.isEmpty()) {
                            Snackbar snack = Snackbar.make(findViewById(android.R.id.content), snackbar, Snackbar.LENGTH_LONG);
                            View view = snack.getView();
                            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
                            view.setLayoutParams(params);
                            snack.show();
                            collapseButton();
                        } else {
                            passwd(username, s_alasan);
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        collapseButton();
                    }
                }).create().show();
    }

    //send password to webserver
    public void passwd(final String username, final String password) {
        class GetDataJSON extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("username", username));
                nameValuePairs.add(new BasicNameValuePair("password", password));
                nameValuePairs.add(new BasicNameValuePair("device", "android"));

                InputStream inputStream = null;
                String result = null;
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(Config.LOGIN);
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();

                    inputStream = entity.getContent();

                    Log.e("pass 1", "connection success ");
                } catch (Exception e) {
                    Log.e("Fail 1", e.toString());
                }

                try {
                    BufferedReader reader = new BufferedReader
                            (new InputStreamReader(inputStream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    inputStream.close();
                    result = sb.toString();
                    result_passwd = result;
                    Log.e("pass 2", "connection success : " + result);
                } catch (Exception e) {
                    Log.e("Fail 2", e.toString());
                }
                Log.e("Result ", result);
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                result_passwd = result;

                action(password);
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }

    //process result from webserver
    protected void action(String alasan) {
        String c = Character.toString(result_passwd.charAt(0));

        if (c.equals("0")) {
            Toast.makeText(getApplicationContext(), "Password yang anda masukkan salah", Toast.LENGTH_SHORT).show();
            collapseButton();
        } else {
            finish(rute_canvassing_id, "4", alasan);
        }
    }

    //checking connection internet
    boolean isConnectInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            return true;
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Koneksi terputus, hubungkan ke jaringan!")
                    .setPositiveButton("Ok", null)
                    .create().show();
            return false;
        }
    }

    //set network on main thread
    void allowNetworkOnMainThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    //action back button action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent i = new Intent(ActivityMaps.this, ActivityDetail.class);
            i.putExtra("rute_canvassing_id", rute_canvassing_id);
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        }
        return super.onOptionsItemSelected(item);
    }

    //action back button phone
    @Override
    public void onBackPressed() {
        if (mode == 0) {
            Intent i = new Intent(ActivityMaps.this, ActivityDetail.class);
            i.putExtra("rute_canvassing_id", rute_canvassing_id);
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        } else {
            new AlertDialog.Builder(this)
                    .setMessage("Sedang melakukan perjalanan, jangan tutup aplikasi")
                    .setNegativeButton("Ya", null)
                    .show();
        }
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(getResources().getColor(R.color.colorAccent));
            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                mMap.addPolyline(lineOptions);
                Log.d("Polyline :", "Tergambar");
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }
}
