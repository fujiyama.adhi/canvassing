package com.quick.canvassing3.Config;

public class Config {
    //link local server
    public static final String LOGIN = "http://192.168.168.115/canvassing/home";
//    public static final String GET_LIST = "http://192.168.18.132/canvassing/employee/C_Employee/realization";
//    public static final String GET_DETAIL = "http://192.168.18.132/canvassing/employee/C_Employee/realization_detail";
//    public static final String INSERT_OTW = "http://192.168.18.132/canvassing/employee/C_Employee/realization_otw";
//    public static final String UPLOAD_LOCATION = "http://192.168.18.132/canvassing/employee/C_Employee/realization_map";
//    public static final String FINISH = "http://192.168.18.132/canvassing/employee/C_Employee/realization_finish";

    //link quick.com
//    public static final String LOGIN = "http://quick.com/aplikasi/canvassing/home";
//    public static final String GET_LIST = "http://quick.com/aplikasi/canvassing/realization";
//    public static final String GET_DETAIL = "http://quick.com/aplikasi/canvassing/realization/detail";
//    public static final String INSERT_OTW = "http://quick.com/aplikasi/canvassing/employee/C_Employee/realization_otw";
//    public static final String UPLOAD_LOCATION = "http://quick.com/aplikasi/canvassing/employee/C_Employee/realization_map";
//    public static final String FINISH = "http://quick.com/aplikasi/canvassing/employee/C_Employee/realization_finish";

    //link quick.co.id
//    public static final String LOGIN = "http://quick.co.id/canvassing/home";
    public static final String GET_LIST = "http://quick.co.id/canvassing/realization";
    public static final String GET_DETAIL = "http://quick.co.id/canvassing/realization/detail";
    public static final String INSERT_OTW = "http://quick.co.id/canvassing/employee/C_Employee/realization_otw";
    public static final String UPLOAD_LOCATION = "http://quick.co.id/canvassing/employee/C_Employee/realization_map";
    public static final String FINISH = "http://quick.co.id/canvassing/employee/C_Employee/realization_finish";
}
