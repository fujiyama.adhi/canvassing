package com.quick.canvassing3;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Admin on 3/12/2018.
 */

public class spHelp {

    public static final String SP_USER_APP = "spUserApp";

    public static final String SP_ID = "id_user";
    public static final String SP_NAMA = "nama_user";
    public static final String SP_USERNAME = "username";
    public static final String SP_PASSWORD = "password";
    public static final String SP_LAT = "latitude";
    public static final String SP_LNG = "longitude";
    public static final String SP_SUDAH_LOGIN = "spSudahLogin";
    public static final String SP_SUDAH_MANGKAT = "spSudahMangkat";
    public static final String SP_DURATION = "duration";

    SharedPreferences sp;
    SharedPreferences.Editor spEditor;

    public spHelp(Context context) {
        sp = context.getSharedPreferences(SP_USER_APP, Context.MODE_PRIVATE);
        spEditor = sp.edit();
    }

    public void saveSPString(String keySP, String value) {
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSPInt(String keySP, int value) {
        spEditor.putInt(keySP, value);
        spEditor.commit();
    }

    public void saveSPBoolean(String keySP, boolean value) {
        spEditor.putBoolean(keySP, value);
        spEditor.commit();
    }

    public String getSPNama() {
        return sp.getString(SP_NAMA, "");
    }

    public int getSPDurasi() {
        return sp.getInt(SP_DURATION, 0);
    }

    public String getSPUsername() {
        return sp.getString(SP_USERNAME, "");
    }

    public String getSPPassword() {
        return sp.getString(SP_PASSWORD, "");
    }

    public String getSpLat() {
        return sp.getString(SP_LAT, "");
    }

    public String getSpLng() {
        return sp.getString(SP_LNG, "");
    }

    public String getSPID() {
        return sp.getString(SP_ID, "");
    }

    public Boolean getSPSudahLogin() {
        return sp.getBoolean(SP_SUDAH_LOGIN, false);
    }

    public Boolean getSPSudahMangkat() {
        return sp.getBoolean(SP_SUDAH_MANGKAT, false);
    }
}
