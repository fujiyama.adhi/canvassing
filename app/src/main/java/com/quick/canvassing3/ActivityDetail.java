package com.quick.canvassing3;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.quick.canvassing3.Config.Config;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ActivityDetail extends AppCompatActivity {
    Button bt_start, bt_preview;
    int mode;
    ProgressBar pb_detail;
    ListView lv_desty, lv_pesertaDL;
    String rute_canvassing_id, data_result;
    JSONArray resultJson1 = null, resultJson2 = null;
    ArrayList<String> alias, alamat, desty_display, latitude, longitude, noind, nama, name_display;
    ArrayAdapter<String> adapter1, adapter2;
    InputStream is = null;
    String result = null, line = null;
    Boolean connect_status = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        allowNetworkOnMainThread();
        //get value id rute
        rute_canvassing_id = getIntent().getStringExtra("rute_canvassing_id");
        //init ui
        bt_start = (Button) findViewById(R.id.bt_start);
        bt_preview = (Button) findViewById(R.id.bt_preview);
        lv_desty = (ListView) findViewById(R.id.lv_desty);
        lv_pesertaDL = (ListView) findViewById(R.id.lv_pesertaDL);
        pb_detail = (ProgressBar) findViewById(R.id.pb_detail);
        //set back button on action bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        //set title on action bar
        setTitle("Detail Rute " + rute_canvassing_id);
        //listerner for start button
        bt_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(ActivityDetail.this)
                        .setMessage("Anda yakin ingin memilih rute ini?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                insertOtw(rute_canvassing_id, "5");
                            }
                        })
                        .setNegativeButton("Tidak", null)
                        .show();
            }
        });
        //listener for preview maps
        bt_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pb_detail.setVisibility(View.VISIBLE);
                mode = 0;
                pindah(mode);
            }
        });
        //listener for list destination
        lv_desty.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogReview(position);
            }
        });
        //listener for list member
        lv_pesertaDL.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        //start function get data from database
        ConnectionCheck();
    }

    //function intent condition
    void pindah(int mode) {
        //init intent
        Intent i = new Intent(ActivityDetail.this, ActivityMaps.class);
        //put value to pass with intent
        i.putExtra("rute_canvassing_id", rute_canvassing_id);
        i.putExtra("mode", mode);

        i.putStringArrayListExtra("alias", alias);
        i.putStringArrayListExtra("alamat", alamat);
        i.putStringArrayListExtra("display", desty_display);
        i.putStringArrayListExtra("latitude", latitude);
        i.putStringArrayListExtra("longitude", longitude);
        //start intent
        startActivity(i);
        //close this activity
        finish();
        //animate transition activity
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    void ConnectionCheck(){
        connect_status = null;
        try {
            connect_status = getServerContentsJson();
        } catch (IOException e) {
            Toast.makeText(this, " Eror", Toast.LENGTH_SHORT).show();
        }
        Log.e("STATUS UTAMA", "" + connect_status);

        if (connect_status){
//            a_anim_noint.setVisibility(View.GONE);
//            a_anim_noapp.setVisibility(View.GONE);
//            a_tv_swipe.setVisibility(View.GONE);
            getList();
        }else {
//            a_anim_noint.setVisibility(View.VISIBLE);
//            a_anim_noapp.setVisibility(View.GONE);
//            a_tv_swipe.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Koneksi Error", Toast.LENGTH_SHORT).show();
            pb_detail.setVisibility(View.GONE);
        }
    }

    private Boolean getServerContentsJson()
            throws IOException {
        HttpClient httpclient = new DefaultHttpClient();

        Boolean responseString = null;
        HttpResponse response = null;
        try {
            response = httpclient.execute(new HttpGet(Config.GET_LIST));

            StatusLine statusline = response.getStatusLine();

            if (statusline.getStatusCode() == HttpStatus.SC_OK) {
                responseString = true;
                return responseString;
            } else {
                responseString = false;
            }
        } catch (IOException e) {
            responseString = false;
        } finally {
            if (response != null) {
                response.getEntity().consumeContent();
            }
        }
        return responseString;
    }

    //function post to get value
    void getList() {
        class GetDataJSON extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("id_rute", rute_canvassing_id));

                String result = null;
                InputStream is = null;
                String line;
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(Config.GET_DETAIL);
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                    Log.e("pass 1", "connection success ");
                } catch (Exception e) {
                    Log.e("Fail 1", e.toString());
                }
                try {
                    BufferedReader reader = new BufferedReader
                            (new InputStreamReader(is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    result = sb.toString();
                    Log.e("pass 2", "connection status : " + result);
                } catch (Exception e) {
                    Log.e("Fail 2", e.toString());
                }
                Log.e("Result", result);
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                data_result = result;
                if (data_result.length() > 1) {
                    save_object();
                } else {
                    Toast.makeText(ActivityDetail.this, "KOSONG", Toast.LENGTH_SHORT).show();
                }
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }

    //function convert json object to array
    void save_object() {
        alias = new ArrayList<String>();
        alamat = new ArrayList<String>();
        latitude = new ArrayList<String>();
        longitude = new ArrayList<String>();
        desty_display = new ArrayList<String>();
        noind = new ArrayList<String>();
        nama = new ArrayList<String>();
        name_display = new ArrayList<String>();
        try {
            JSONObject jsonObj = new JSONObject(data_result);
            resultJson1 = jsonObj.getJSONArray("destinasi");
            for (int i = 0; i < resultJson1.length(); i++) {
                JSONObject c = resultJson1.getJSONObject(i);
                Log.d("DES", c.getString("alias"));
                alias.add(c.getString("alias"));
                alamat.add(c.getString("alamat"));
                latitude.add(c.getString("latitude"));
                longitude.add(c.getString("longitude"));
//
                desty_display.add("" + (i + 1) + " - " + c.getString("alias"));
            }
            resultJson2 = jsonObj.getJSONArray("peserta");
            for (int i = 0; i < resultJson2.length(); i++) {
                JSONObject d = resultJson2.getJSONObject(i);
                noind.add(d.getString("no_ind"));
                nama.add(d.getString("nama"));

                name_display.add("" + (i + 1) + " - " + d.getString("no_ind") + " " + d.getString("nama"));
            }
            if (desty_display != null && name_display != null) {
                adapter1 = new ArrayAdapter<>(this, R.layout.row_desty, desty_display);
                adapter2 = new ArrayAdapter<>(this, R.layout.row_desty, name_display);
                lv_desty.setAdapter(adapter1);
                lv_pesertaDL.setAdapter(adapter2);
                pb_detail.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //function post value to database
    void insertOtw(String rute_id, String status) {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        nameValuePairs.add(new BasicNameValuePair("rute_id", rute_id));
        nameValuePairs.add(new BasicNameValuePair("status", status));

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.INSERT_OTW);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString());
        }

        try {
            BufferedReader reader = new BufferedReader
                    (new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection status : " + result);
        } catch (Exception e) {
            Log.e("Fail 2", e.toString());
        }
        hasil(result);
    }

    //get result from insert otw
    void hasil(String result) {
        if (result.length() == 1) {
            Toast.makeText(this, "Koneksi bermasalah, silahkan ulangi lagi", Toast.LENGTH_SHORT).show();
        } else {
            pb_detail.setVisibility(View.VISIBLE);
            mode = 1;
            pindah(mode);
        }
    }

    //set network on main thread
    void allowNetworkOnMainThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    //dialog detail from client
    void dialogReview(int i) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View viewInflater = inflater.inflate(R.layout.dialog_desty, null, false);
        final TextView tv_alias = (TextView) viewInflater.findViewById(R.id.tv_alias);
        final TextView tv_alamat = (TextView) viewInflater.findViewById(R.id.tv_alamat);
        final TextView tv_lat = (TextView) viewInflater.findViewById(R.id.tv_lat);
        final TextView tv_lng = (TextView) viewInflater.findViewById(R.id.tv_lng);

        tv_alias.setText(": " + alias.get(i));
        tv_alamat.setText(": " + alamat.get(i));
        tv_lat.setText(": " + latitude.get(i));
        tv_lng.setText(": " + longitude.get(i));


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Destinasi " + (i + 1))
                .setView(viewInflater)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).create().show();
    }

    //action back button action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        }
        return super.onOptionsItemSelected(item);
    }

    //action back button phone
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
}