package com.quick.canvassing3;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.quick.canvassing3.Adapter.RecyclerViewAdapter;
import com.quick.canvassing3.Config.Config;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ActivityMain extends AppCompatActivity implements RecyclerViewAdapter.ItemClickListener {
    SwipeRefreshLayout swipe_refresh;
    spHelp SP_Helper;
    String username, nama, data_result;
    JSONArray resultJson = null;
    TextView tv_no_induk, tv_swipe, tv_noInternet;
    ArrayList<String> rute_canvassing_id, destination;
    Boolean connect_status = null;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    int durasi = 0;

    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mmAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        allowNetworkOnMainThread();

        //get value
        SP_Helper = new spHelp(this);
        username = SP_Helper.getSPUsername();
        nama = SP_Helper.getSPNama();
        durasi = SP_Helper.getSPDurasi();

        Log.e("NAMA", ""+nama);
        //init ui
        tv_no_induk = (TextView) findViewById(R.id.tv_no_induk);
        tv_swipe = (TextView) findViewById(R.id.tv_swipe);
        tv_noInternet = (TextView) findViewById(R.id.tv_noInternet);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_main);
        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        //listene for swipe refresh layotout
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ConnectionCheck();
//                getList();
            }
        });
        //set setting recycleview
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mLayoutManager);
        //set identity user
        tv_no_induk.setText(username.toUpperCase() + " - " + nama);

        requestPermissions();
        //call function get value list
        ConnectionCheck();
//        getList();
    }

//    @Override
//    public void onResume() {
//        super.onResume();
////        getList();
//        ConnectionCheck();
////        Toast.makeText(this, username, Toast.LENGTH_SHORT).show();
//    }

    void ConnectionCheck(){
        connect_status = null;
        try {
            connect_status = getServerContentsJson();
        } catch (IOException e) {
            Toast.makeText(this, " Eror", Toast.LENGTH_SHORT).show();
        }
        Log.e("STATUS UTAMA", "" + connect_status);

        if (connect_status){
            mRecyclerView.setVisibility(View.VISIBLE);
            tv_noInternet.setVisibility(View.GONE);
//            a_anim_noint.setVisibility(View.GONE);
//            a_anim_noapp.setVisibility(View.GONE);
//            a_tv_swipe.setVisibility(View.GONE);
            getList();
        }else {
            mRecyclerView.setVisibility(View.GONE);
            tv_noInternet.setVisibility(View.VISIBLE);
            tv_swipe.setVisibility(View.VISIBLE);
//            a_anim_noint.setVisibility(View.VISIBLE);
//            a_anim_noapp.setVisibility(View.GONE);
//            a_tv_swipe.setVisibility(View.VISIBLE);
            swipe_refresh.setRefreshing(false);
            Toast.makeText(this, "Koneksi Error", Toast.LENGTH_SHORT).show();
        }
    }

    private Boolean getServerContentsJson()
            throws IOException {
        HttpClient httpclient = new DefaultHttpClient();

        Boolean responseString = null;
        HttpResponse response = null;
        try {
            response = httpclient.execute(new HttpGet(Config.GET_LIST));

            StatusLine statusline = response.getStatusLine();

            if (statusline.getStatusCode() == HttpStatus.SC_OK) {
                responseString = true;
                return responseString;
            } else {
                responseString = false;
            }
        } catch (IOException e) {
            responseString = false;
        } finally {
            if (response != null) {
                response.getEntity().consumeContent();
            }
        }
        return responseString;
    }

    //function get value for list
    void getList() {
        class GetDataJSON extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("noind", username));

                String result = null;
                InputStream is = null;
                String line;
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(Config.GET_LIST);
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                    Log.e("pass 1", "connection success ");
                } catch (Exception e) {
                    Log.e("Fail 1", e.toString());
                }
                try {
                    BufferedReader reader = new BufferedReader
                            (new InputStreamReader(is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    result = sb.toString();
                    Log.e("pass 2", "connection status : " + result);
                } catch (Exception e) {
                    Log.e("Fail 2", e.toString());
                }
                Log.e("Result", result);
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                data_result = result;
                Log.e("DATA", ""+data_result.length());
                if (data_result.length() > 2) {
                    save_object();
                } else {
                    Toast.makeText(ActivityMain.this, "Tidak ada rencana perjalanan", Toast.LENGTH_SHORT).show();
                    swipe_refresh.setRefreshing(false);
                    tv_swipe.setVisibility(View.VISIBLE);
                }
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }

    //function convert json object to array
    void save_object() {
        rute_canvassing_id = new ArrayList<String>();
        destination = new ArrayList<String>();
        try {
            JSONObject jsonObj = new JSONObject(data_result);
            resultJson = jsonObj.getJSONArray("result");

            for (int i = 0; i < resultJson.length(); i++) {
                JSONObject c = resultJson.getJSONObject(i);

                rute_canvassing_id.add(c.getString("rute_canvassing_id"));
                destination.add(c.getString("destination"));
            }
            Log.d("RUTE CANVASSING", "" + rute_canvassing_id.size());

            if (rute_canvassing_id != null) {
                mmAdapter = new RecyclerViewAdapter(this, rute_canvassing_id, destination);
                mRecyclerView.setAdapter(mmAdapter);
                swipe_refresh.setRefreshing(false);
                tv_swipe.setVisibility(View.GONE);
            } else {
                Toast.makeText(this, "Can't load data", Toast.LENGTH_SHORT).show();
                swipe_refresh.setRefreshing(false);
                tv_swipe.setVisibility(View.VISIBLE);
            }

            // Stop refresh animation
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void requestPermissions() {

        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION);

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);


        // Provide an additional rationale to the img_user. This would happen if the img_user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale || shouldProvideRationale2) {
            showSnackbar(R.string.String_permission,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(ActivityMain.this,
                                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the img_user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(ActivityMain.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    void allowNetworkOnMainThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    //create option menu to logout
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_awal, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //handle action logout
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_logout) {
            new AlertDialog.Builder(this)
                    .setMessage("Anda yakin ingin logout?")
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SP_Helper.saveSPBoolean(spHelp.SP_SUDAH_LOGIN, false);
                            Intent i = new Intent(ActivityMain.this, ActivityLogin.class);
                            startActivity(i);
                            finish();
                        }
                    })
                    .setNegativeButton("Tidak", null)
                    .show();
        }
        return super.onOptionsItemSelected(item);
    }

    //listener for ricyclerview
    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        Intent i = new Intent(ActivityMain.this, ActivityDetail.class);
        i.putExtra("rute_canvassing_id", rute_canvassing_id.get(position));
        startActivity(i);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    //action for backpress button android
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Anda yakin ingin menutup aplikasi ini?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }
}
